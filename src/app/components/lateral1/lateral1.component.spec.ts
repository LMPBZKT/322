import { ComponentFixture, TestBed } from '@angular/core/testing';

import { Lateral1Component } from './lateral1.component';

describe('Lateral1Component', () => {
  let component: Lateral1Component;
  let fixture: ComponentFixture<Lateral1Component>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ Lateral1Component ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(Lateral1Component);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
